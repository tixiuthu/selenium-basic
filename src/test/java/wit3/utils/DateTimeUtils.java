package wit3.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;


public class DateTimeUtils {

    private static Configuration configuration = Configuration.get();

    public static String getDateTimeFormattedWithTimeZone(Calendar calendar, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(configuration.getTestProperty("timezone")));
        return simpleDateFormat.format(calendar.getTime());
    }

    public static Calendar createCalendarBaseTimestamp(long timeInMillis) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(configuration.getTestProperty("timezone")));
        calendar.setTimeInMillis(timeInMillis);
        return calendar;
    }

}
