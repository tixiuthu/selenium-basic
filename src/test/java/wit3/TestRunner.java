package wit3;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by PC on 11/04/2017.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/",
        format = {"pretty", "html:target/cucumber-htmlreport", "json:target/cucumber-report.json"}
)
public class TestRunner {
}
