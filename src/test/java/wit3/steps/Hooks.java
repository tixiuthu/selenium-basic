package wit3.steps;

import cucumber.api.java.Before;
import wit3.core.Driver;

public class Hooks {

    @Before("@ResetWeb")
    public void resetApp() {
        Driver.setNoReset(false);
    }
}
