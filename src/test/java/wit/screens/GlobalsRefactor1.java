package wit.screens;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import wit.core.Driver;

import java.util.ArrayList;
import java.util.List;

public class GlobalsRefactor1 {

    private static String TITLE_GLOBAL_WS = "Registration | Demoqa";


    public static void openMagicBricks() throws InterruptedException {
        Driver.setDriver();
        Driver.getDriver().get("http://demoqa.com/registration/");
        Thread.sleep(2000);
    }

    public static void isMagicBricksPage(String expectedTitle) {

        //Getting the actual Title
        String actualTitle = "";
        actualTitle = Driver.getDriver().getTitle();//selenium WebDriver.//cau lenh lay title cua trang web do
        //Validating the test case
        Assert.assertEquals(expectedTitle, actualTitle);

    }

    public static void inputValuesInsideForm() throws InterruptedException {
        WebElement keywordElement = Driver.getDriver().findElement(By.id("name_3_firstname"));//driver.
        keywordElement.sendKeys("Thuy Nguyen");
        Thread.sleep(3000);
        //  Driver.closeDriver();
        List<WebElement> listStatus = new ArrayList<WebElement>();
        listStatus = Driver.getDriver().findElements(By.name("radio_4[]"));
        listStatus.get(1).click();
        Thread.sleep(2000);

        //   ((JavascriptExecutor)
        //      Driver.getDriver()).executeScript("scroll(0,1000)");

        WebElement scroll = Driver.getDriver().findElement(By.id("email_1"));
        JavascriptExecutor js = (JavascriptExecutor) Driver.getDriver();
        js.executeScript("arguments[0].scrollIntoView(true);", scroll);
        Thread.sleep(3000);
    }

    public static void main(String[] args) throws InterruptedException {

        //open Bank Manage WWebsite
        openMagicBricks();
        isMagicBricksPage(TITLE_GLOBAL_WS);
        inputValuesInsideForm();

    }


}
