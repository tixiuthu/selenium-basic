


import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class BankManager {
    public static void main(String[] args) throws InterruptedException {

        WebDriver driver;
        String Firefoxdriverpath = System.getProperty("user.dir") + "/src/test/resources/driver/geckodriver";
        System.setProperty("webdriver.gecko.driver", Firefoxdriverpath);

// create a new instance of the Firefox driver
        driver = new FirefoxDriver();

// Open the page we want to open
        driver.get("http://demo.guru99.com/V1/html/Managerhomepage.php");

//Defining Expected Title
        String expectedTitle = "GTPL Bank Manager HomePage";
//Getting the actual Title
        String actualTitle = null;
        actualTitle = driver.getTitle();

//Validating the test case
        if (actualTitle.contentEquals(expectedTitle)) {
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }
        Thread.sleep(2000);

        // Add customer
        driver.navigate().to("http://demo.guru99.com/V1/html/addcustomerpage.php");
        Thread.sleep(2000);

        WebElement name = driver.findElement(By.name("name"));
        name.clear();
        name.sendKeys("ThuyTien");
        Thread.sleep(2000);

        //  WebElement gender = driver.findElement(By.cssSelector(".layout > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(2) > input:nth-child(2)"));
        WebElement gender = driver.findElement(By.xpath("//tr/td/table/tbody/tr[5]/td[2]/input[2]"));
        gender.click();
        Thread.sleep(2000);

        driver.switchTo().activeElement().sendKeys(Keys.TAB, Keys.TAB, "5", "5", "1990");
        Thread.sleep(2000);

        //  WebElement Andress = driver.findElement(By.cssSelector(".layout > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(7) > td:nth-child(2) > textarea:nth-child(1)"));
        WebElement Andress = driver.findElement(By.xpath("//tr/td/table/tbody/tr[7]/td[2]/textarea"));
        Andress.clear();
        Andress.sendKeys("VietNam");
        Thread.sleep(2000);

        WebElement city = driver.findElement(By.name("city"));
        city.clear();
        city.sendKeys("DaNang");
        Thread.sleep(2000);

        WebElement state = driver.findElement(By.name("state"));
        state.clear();
        state.sendKeys("daucungduoc");
        Thread.sleep(2000);

        WebElement pin = driver.findElement(By.name("pinno"));
        pin.clear();
        pin.sendKeys("123456");
        Thread.sleep(2000);

        WebElement phone = driver.findElement(By.name("telephoneno"));
        phone.clear();
        phone.sendKeys("0123456789");
        Thread.sleep(2000);

        WebElement email = driver.findElement(By.name("emailid"));
        email.clear();
        email.sendKeys("test1@gmail.com");
        email.sendKeys(Keys.ENTER);
        Thread.sleep(3000);

        // Edit customer
        driver.navigate().to("http://demo.guru99.com/V1/html/EditCustomer.php");
        Thread.sleep(2000);
        WebElement cusid = driver.findElement(By.name("cusid"));
        cusid.clear();
        cusid.sendKeys("1234567890");
        WebElement sub = driver.findElement(By.name("AccSubmit"));
        sub.click();
        Thread.sleep(3000);

        //Delete Customer
        driver.navigate().to("http://demo.guru99.com/V1/html/DeleteCustomerInput.php");
        Thread.sleep(2000);
        WebElement cusde = driver.findElement(By.name("cusid"));
        cusde.clear();
        cusde.sendKeys("1234567890");
        WebElement subde = driver.findElement(By.name("AccSubmit"));
        subde.click();
        Thread.sleep(3000);

        // Add Account
        driver.navigate().to("http://demo.guru99.com/V1/html/addAccount.php");
        Thread.sleep(2000);
        WebElement cusacc = driver.findElement(By.name("cusid"));
        cusacc.clear();
        cusacc.sendKeys("1234567890");
        Thread.sleep(2000);

        Select acctype = new Select(driver.findElement(By.name("selaccount")));
        acctype.selectByValue("current");
        Thread.sleep(2000);

        WebElement initial = driver.findElement(By.name("inideposit"));
        initial.clear();
        initial.sendKeys("123123123");
        Thread.sleep(2000);

        WebElement subadd = driver.findElement(By.name("button2"));
        subadd.click();
        Thread.sleep(3000);

        // Edit Account
        driver.navigate().to("http://demo.guru99.com/V1/html/editAccount.php");
        Thread.sleep(2000);
        WebElement accno = driver.findElement(By.name("accountno"));
        accno.clear();
        accno.sendKeys("9876543210");
        Thread.sleep(2000);
        WebElement subedit = driver.findElement(By.name("AccSubmit"));
        subedit.click();
        Thread.sleep(3000);

        //Delete Account
        driver.navigate().to("http://demo.guru99.com/V1/html/deleteAccountInput.php");
        Thread.sleep(2000);
        WebElement accde = driver.findElement(By.name("accountno"));
        accde.clear();
        accde.sendKeys("98754387623");
        Thread.sleep(2000);
        WebElement subdeac = driver.findElement(By.name("AccSubmit"));
        subdeac.click();
        Thread.sleep(3000);

        //Mini Statement
        driver.navigate().to("http://demo.guru99.com/V1/html/MiniStatementInput.php");
        Thread.sleep(2000);
        WebElement accmini = driver.findElement(By.name("accountno"));
        accmini.clear();
        accmini.sendKeys("1234587659");
        WebElement submini = driver.findElement(By.name("AccSubmit"));
        submini.click();
        Thread.sleep(3000);

        //Customized Statement
        driver.navigate().to("http://demo.guru99.com/V1/html/CustomisedStatementInput.php");
        WebElement acccus = driver.findElement(By.name("accountno"));
        acccus.clear();
        acccus.sendKeys("123498765");
        driver.switchTo().activeElement().sendKeys(Keys.TAB, "5", "12", "2011", Keys.TAB, "5", "12", "2018");
        Thread.sleep(2000);
        WebElement limit = driver.findElement(By.name("loweramt"));
        limit.clear();
        limit.sendKeys("1000");
        Thread.sleep(2000);
        WebElement transaction = driver.findElement(By.name("tranno"));
        transaction.clear();
        transaction.sendKeys("4321876");
        WebElement subst = driver.findElement(By.name("AccSubmit"));
        subst.click();
        Thread.sleep(3000);

        driver.close();

        //driver.quit();

    }
}

