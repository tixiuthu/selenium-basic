package wit2.screens.user;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import wit2.core.Driver;


/**
 * Created by thuynguyen on 9/15/18.
 */
public class UserScreen {


    private static WebDriver driver;
    private static UserScreen INSTANCE;
    private static WebDriverWait wait;

    private UserScreen() {
    }

    private UserScreen getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UserScreen();
            driver = Driver.getDriver();
        }
        return INSTANCE;
    }

    private void initHomePage() {
        Driver.openDriver();
        driver = Driver.getDriver();
        PageFactory.initElements(driver, this);
        wait = Driver.getWait();

    }

    public void openWebsite() {
        driver.get("http://www.globalsqa.com/angularJs-protractor/BankingProject/#/login");
    }



    public static void main(String args[]){

        UserScreen userScreen = new UserScreen();
        userScreen.getInstance();
        userScreen.initHomePage();
        userScreen.openWebsite();
        //  driver.close();
    }

}
