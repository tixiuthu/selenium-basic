package wit2.screens.user;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import wit2.core.Driver;
import wit2.screens.home.HomeElement;

public class CustomerScreen extends HomeElement {

    private static WebDriver driver;
    private static CustomerScreen INSTANCE;
    private static WebDriverWait wait;

    private CustomerScreen() {
    }

    private CustomerScreen getInstance() {
        if (INSTANCE == null || (driver != null && Driver.getDriver() != driver)) {
            INSTANCE = new CustomerScreen();
            driver = Driver.getDriver();
        }
        return INSTANCE;
    }

    private void initHomePage() {
        Driver.openDriver();
        driver = Driver.getDriver();
        PageFactory.initElements(driver, this);
        wait = Driver.getWait();

    }

    public void openWebsite(){
        driver.get("http://www.globalsqa.com/samplepagetest/");
    }

    public void enterName() {
        wait.until(ExpectedConditions.visibilityOf(name));
        name.sendKeys("Thuy Tien");

    }
    public void closeWebsite(){
        driver.close();

    }

    public static void main (String arg[]){
        CustomerScreen homeScreen = new CustomerScreen();
        homeScreen.getInstance();
        homeScreen.initHomePage();
        homeScreen.openWebsite();
        homeScreen.enterName();
        homeScreen.closeWebsite();
    }




}

