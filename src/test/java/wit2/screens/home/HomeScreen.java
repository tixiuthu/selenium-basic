package wit2.screens.home;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import wit2.core.Driver;

public class HomeScreen extends HomeElement {

    private static WebDriver driver;
    private static HomeScreen INSTANCE;
    private static WebDriverWait wait;

    private HomeScreen() {
    }

    private HomeScreen getInstance() {
        if (INSTANCE == null || (driver != null && Driver.getDriver() != driver)) {
            INSTANCE = new HomeScreen();
            driver = Driver.getDriver();
        }
        return INSTANCE;
    }

    private void initHomePage() {
        Driver.openDriver();
        driver = Driver.getDriver();
        PageFactory.initElements(driver, this);
        wait = Driver.getWait();

    }

    public void openWebsite(){
        driver.get("http://www.globalsqa.com/samplepagetest/");
    }

    public void enterName() {
        wait.until(ExpectedConditions.visibilityOf(name));
        name.sendKeys("Thuy Tien");

    }
    public void closeWebsite(){
        driver.close();

    }

    public static void main (String arg[]){
        HomeScreen homeScreen = new HomeScreen();
        homeScreen.getInstance();
        homeScreen.initHomePage();
        homeScreen.openWebsite();
        homeScreen.enterName();
        homeScreen.closeWebsite();
    }




}

