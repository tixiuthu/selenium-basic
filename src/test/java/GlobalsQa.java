

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class GlobalsQa {
    public static void main(String[] args) throws InterruptedException {


        String Firefoxdriverpath = System.getProperty("user.dir") + "/src/driver/geckodriver";
        System.setProperty("webdriver.gecko.driver", Firefoxdriverpath);

        WebDriver driver;
// create a new instance of the Firefox driver
        driver = new FirefoxDriver();

// Open the page we want to open
        driver.get("http://www.globalsqa.com/samplepagetest/");
        driver.manage().window().maximize();
//Defining Expected Title
        String expectedTitle = "Sample Page Testing Site Online - Global SQA Testing Sites";
//Getting the actual Title
        String actualTitle = null;
        actualTitle = driver.getTitle();

//Validating the test case
        if (actualTitle.contentEquals(expectedTitle)) {
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }

        WebElement profile = driver.findElement(By.cssSelector(".wpcf7-form-control"));
        profile.clear();
        String image_path = System.getProperty("user.dir") + "/src/images/biking.jpg";
        profile.sendKeys(image_path);
        Thread.sleep(2000);

        WebElement name = driver.findElement(By.id("g2599-name"));
        name.clear();
        name.sendKeys("Thuy Tien");
        Thread.sleep(2000);

        WebElement email = driver.findElement(By.id("g2599-email"));
        email.clear();
        email.sendKeys("test1@gmail.com");
        Thread.sleep(2000);

        WebElement website = driver.findElement(By.id("g2599-website"));
        website.clear();
        website.sendKeys("kenh14.vn");
        Thread.sleep(2000);

        Select expe = new Select(driver.findElement(By.id("g2599-experienceinyears")));
        expe.selectByValue("3-5");
        Thread.sleep(3000);

        WebElement scroll = driver.findElement(By.id("g2599-experienceinyears"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", scroll);
        Thread.sleep(3000);

        WebElement checkfunction = driver.findElement(By.cssSelector("label.grunion-checkbox-multiple-label:nth-child(2)"));
        checkfunction.click();

        WebElement checkauto = driver.findElement(By.cssSelector("label.grunion-checkbox-multiple-label:nth-child(4)"));
        checkauto.click();

        WebElement checkmanual = driver.findElement(By.cssSelector("label.grunion-checkbox-multiple-label:nth-child(6)"));
        checkmanual.click();

        Thread.sleep(2000);

        WebElement tickeducation = driver.findElement(By.cssSelector("label.grunion-radio-label:nth-child(6)"));
        tickeducation.click();
        Thread.sleep(2000);

        //click alert box
        // dau tien tim thang cha cua button, va tim cho nao co ID thi cang tot
        //vi vay minh tim id truoc

        WebElement formElement = driver.findElement(By.id("contact-form-2599"));
        //minh thay trong form co 2 button trong form nhung button cua minh can tim la vi tri thu 1 ( trong list thif la 0)
        List<WebElement> list = formElement.findElements(By.tagName("button"));
        list.get(0).click();
        Thread.sleep(2000);
        // Accepts (Click on OK) Chrome Alert Browser for RESET button.
        //popup hien thi 2 lan , 1 lan la cancel,ok . 1 lan sau la ok prevent vi vay minh phai su dung alertOK.accpept 2 lan
        //chu y cai nay chay debug de de hieu hon

        Alert alertOK = driver.switchTo().alert();
        alertOK.accept();
        alertOK.accept();

//Rejects (Click on Cancel) Chrome Browser Alert for RESET button.

      /*  Alert alertCancel = driver.switchTo().alert();
        alertCancel.dismiss();*/

        Thread.sleep(5000);
        WebElement comment = driver.findElement(By.id("contact-form-comment-g2599-comment"));
        comment.sendKeys("Comment is here");
        Thread.sleep(2000);

        WebElement submit = driver.findElement(By.cssSelector(".pushbutton-wide"));
        submit.click();
        Thread.sleep(2000);


        driver.close();

        //driver.quit();

    }
}
